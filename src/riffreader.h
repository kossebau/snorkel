/*
    This file is part of the Snorkel program, made within the KDE community.

    Copyright 2012 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RIFFREADER_H
#define RIFFREADER_H

// Koralle
#include <Koralle0/RiffStreamReader>
// KDE
#include <KIcon>

class QTreeWidget;
class QTreeWidgetItem;


class RiffReader
{
public:
    explicit RiffReader( QTreeWidget* treeWidget );

    bool read( QIODevice* device );

    QString errorString() const;

private:
    void readList( QTreeWidgetItem* item );
    void readChunk( QTreeWidgetItem* item );

    QTreeWidgetItem* createChildItem( QTreeWidgetItem* item );

private:
    Koralle::RiffStreamReader mRiffStreamReader;
    QTreeWidget* mTreeWidget;

    KIcon mListIcon;
    KIcon mChunkIcon;
};

#endif

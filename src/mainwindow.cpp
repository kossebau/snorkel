/*
    This file is part of the Snorkel program, made within the KDE community.

    Copyright 2012 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"

// library
#include "riffreader.h"
// KDE
#include <KUrl>
#include <KStatusBar>
// Qt
#include <QFile>
#include <QTreeWidget>
#include <QHeaderView>


MainWindow::MainWindow( const KUrl& url )
  : KMainWindow()
{
    // setup ui
    QStringList labels;
    labels << QLatin1String("Type") << QLatin1String("Size");

    QTreeWidget* treeWidget = new QTreeWidget;
    treeWidget->setHeaderLabels(labels);
    setCentralWidget(treeWidget);

    // load file
    const QString fileName = url.toLocalFile();

    QFile file(fileName);
    if( ! file.open(QFile::ReadOnly) )
    {
        statusBar()->showMessage( QLatin1String("Could not open ") + fileName + QLatin1String(". ") + file.errorString() );
        return;
    }

    RiffReader reader(treeWidget);
    if( ! reader.read(&file) )
    {
        statusBar()->showMessage( QLatin1String("Could not parse ") + fileName + QLatin1String(". ") + reader.errorString() );
        return;
    }

    setWindowTitle( url.fileName() );
    statusBar()->showMessage(QLatin1String("File loaded"));
}

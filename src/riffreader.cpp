/*
    This file is part of the Snorkel program, made within the KDE community.

    Copyright 2012 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "riffreader.h"

// Koralle
#include <Koralle0/FourCharCode>
// Qt
#include <QTreeWidget>


RiffReader::RiffReader( QTreeWidget* treeWidget )
  : mTreeWidget( treeWidget )
  , mListIcon(QLatin1String("folder"))
  , mChunkIcon(QLatin1String("document"))
{
}

QString RiffReader::errorString() const
{
    return mRiffStreamReader.errorString();
}

bool
RiffReader::read( QIODevice* device )
{
    mRiffStreamReader.setDevice( device );

    if( mRiffStreamReader.readNextChunkHeader() )
    {
        if( mRiffStreamReader.isFileChunk() )
            readList( 0 );
        else
            return false;
//             mRiffStreamReader.raiseError( "The file is not a RIFF file." );
    }

    return (! mRiffStreamReader.hasError() );
}

void
RiffReader::readList( QTreeWidgetItem* item )
{
    QTreeWidgetItem* list = createChildItem( item );
    list->setIcon( 0, mListIcon );
    list->setText( 0, mRiffStreamReader.chunkId().toString() );

    mTreeWidget->setItemExpanded( list, true );

    mRiffStreamReader.openList();

    while( mRiffStreamReader.readNextChunkHeader()) {
        if( mRiffStreamReader.isFileChunk() )
            readList(list);
        else if( mRiffStreamReader.isListChunk() )
            readList(list);
        else
            readChunk(list);
    }

    mRiffStreamReader.closeList();
}

void
RiffReader::readChunk( QTreeWidgetItem* item )
{
    QTreeWidgetItem* chunk = createChildItem( item );

    chunk->setIcon( 0, mChunkIcon);
    chunk->setText( 0, mRiffStreamReader.chunkId().toString() );
    chunk->setText( 1, QString::number(mRiffStreamReader.chunkSize()) );
}

QTreeWidgetItem*
RiffReader::createChildItem( QTreeWidgetItem* item )
{
    return item ? new QTreeWidgetItem( item ) : new QTreeWidgetItem( mTreeWidget );
}

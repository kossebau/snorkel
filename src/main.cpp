/*
    This file is part of the Snorkel program, made within the KDE community.

    Copyright 2012 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

// program
#include "mainwindow.h"
// KDE
#include <KAboutData>
#include <KCmdLineArgs>
#include <KLocale>
#include <KApplication>
#include <KUrl>


int main( int argc, char** argv )
{
    KAboutData aboutData("snorkel", 0,
                         ki18n("Snorkel"),
                         "0.1.0",
                         ki18n("RIFF File Viewer"),
                         KAboutData::License_LGPL_V2,
                         ki18n("Copyright 2012 Friedrich W. H. Kossebau"));
    aboutData.setProgramIconName(QLatin1String("package_office_documentviewer"));
    aboutData.addAuthor(ki18n("Friedrich W. H. Kossebau"), KLocalizedString(), "kossebau@kde.org");
    KCmdLineArgs::init(argc, argv, &aboutData);

    KCmdLineOptions options;
    options.add("+in", ki18n("Input file"));
    KCmdLineArgs::addCmdLineOptions(options);

    KApplication app;

    // Get the command line arguments which we have to parse
    KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
    if (args->count() != 1) {
        KCmdLineArgs::usageError(i18n("One arguments required"));
        return 3;
    }

    MainWindow* mainWindow = new MainWindow( args->url(0) ) ;
    mainWindow->show();

    return app.exec();
}

